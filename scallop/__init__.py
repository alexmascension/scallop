from . import tl
from . import pl as pl
from . import pp as pp
from . import datasets as datasets
from .classes.scallop import Scallop
from .classes.bootstrap import Bootstrap

__author__ = ', '.join([
    'Alex M. Ascensión',
    'Olga Ibañez-Solé',
])
__email__ = ', '.join([
    'alexmascension@gmail.com',
    'olga.ibanez@biodonostia.org',
    # We don’t need all, the main authors are sufficient.
])


__version__ = "1.3.0"


